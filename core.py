'''
Fran Truncale
'''
import re
import sys
from functools import reduce

INT = 'INT'; STR='STR'; IDT = 'IDT'; OPR = 'OPR'

lex_prep = [
(r'[ \n\t]+', None), (r'#[^\n]*', None), (r'\:=', OPR),(r'\(', OPR), (r';', OPR), #Syntax
(r'\+', OPR), (r'\)', OPR), (r'\*', OPR), (r'-', OPR), (r'/', OPR), #Arithmetic operators
 (r'<=', OPR), (r'<', OPR), (r'>=', OPR), (r'>', OPR), #Comparative Operators
(r'!=', OPR), (r'=', OPR), (r'and', OPR), (r'or', OPR), (r'not', OPR),  #Logical operators
(r'if', OPR), (r'then', OPR), (r'else', OPR), (r'while', OPR), (r'for', OPR), (r'to', OPR), (r'do', OPR), (r'end', OPR), #Syntax operators
(r'[+-]?\d+(?:\d+)?', INT), (r'".+"', STR), #Primitives
(r'[A-Za-z][A-Za-z0-9_]*', IDT)] #Identifiers

def lex_actual(input, expressions):
	lenlist = 0
	tokens = []
	while lenlist < len(input):
		for lex_prep in expressions:
			lex_gen, lex_tag = lex_prep
			reg_exp = re.compile(lex_gen)
			found = reg_exp.match(input, lenlist)
			if found:
				strng = found.group(0)
				if lex_tag:
					token = (strng, lex_tag)
					tokens.append(token)
				break
		if found: 
			lenlist = found.end(0)
		else:
			print(input[lenlist], "<- Illegal character.")
			sys.exit()
	return tokens

def token_gen(input):
	return lex_actual(input, lex_prep)

class state_contain():
	def __init__(self,state): #catch for sanity check
		self.state = state

class var_assign(state_contain):
	def __init__(self, name, andEx):
		self.name = name
		self.andEx = andEx

	def loop_act(self):
		return (self.name, self.andEx)

	def process(self, val):
		value = self.andEx.process(val)
		val[self.name] = value

#If multiple declared variables in statements
class mult_declare(state_contain):
	def __init__(self, state1, state2):
		self.state1 = state1
		self.state2 = state2

	def loop_act(self):
		return (self.state1, self.state2)

	def process(self, val):
		self.state1.process(val)
		self.state2.process(val)

class if_actual(state_contain):
	def __init__(self, condition, true, false):
		self.condition = condition
		self.true = true
		self.false = false

	def loop_act(self):
		return (self.condition, self.true, self.false)

	def process(self, val):
		boolCondition = self.condition.process(val)
		if boolCondition:
			self.true.process(val)
		else:
			if self.false:
				self.false.process(val)

class for_actual(state_contain): 
	def __init__(self, condition, state, sequence):
		self.condition = condition
		self.state = sequence
		self.sequence = state

	def loop_act(self):
		return (self.condition, self.state, self.sequence)

	def process(self, val):
		forCondition = self.condition.process(val)
		forSequence = self.sequence.process(val)
		iterate = int(forCondition)
		while (iterate < int(forSequence)):
			iterate	+= 1 #Pascal, loops only increment by 1
			self.state.process(val)
			forCondition = self.condition.process(val)
			forSequence = self.sequence.process(val)

class while_actual(state_contain): 
	def __init__(self, condition, state):
		self.condition = condition
		self.state = state

	def loop_act(self):
		return (self.condition, self.state)

	def process(self, val):
		whileCondition = self.condition.process(val)
		while whileCondition:
			self.state.process(val)
			whileCondition = self.condition.process(val)

class string_class(str): #alternative string class, consider moving to module
   def process(self, *arg):
      return self
			
class arth_container():
	def __init__(self,state): #catch for sanity check
		self.state = state

class integer_class(arth_container):
	def __init__(self, int):
		self.int = int

	def ret_exact(self):
		return self.int

	def loop_act(self):
		return self.int

	def process(self, val):
		return self.int

class var_class(arth_container):
	def __init__(self, var):
		self.var = var

	def loop_act(self):
		return self.var

	def process(self, val):
		if self.var in val:
			return val[self.var]
		else:
			return 0

class arith_actual(arth_container):
	def __init__(self, op, left, right):
		self.op = op
		self.left = left
		self.right = right

	def loop_act(self):
		return (self.op, self.left, self.right)

	def process(self, val):
		val_left = self.left.process(val)
		val_right = self.right.process(val)
		def switchcase(argument):
			switch = {
			'+': (val_left + val_right),
			'-': (val_left - val_right),
			'/': (val_left / val_right),
			'*': (val_left * val_right),
			}
			return switch.get(argument,"False")
		casestore = switchcase(self.op)
		
		if (casestore == "False"):
			print ('Unknown operator:', self.op)
			sys.exit()
			
		return casestore

class bool_class():
	def __init__(self,input): #catch for sanity check
		self.input = input

class bool_comparison(bool_class):
	def __init__(self, op, left, right):
		self.op = op
		self.left = left
		self.right = right

	def loop_act(self):
		return (self.op, self.left, self.right)

	def process(self, val):
		val_left = self.left.process(val)
		val_right = self.right.process(val)
		def switchcase(argument):
			switch = {
			'<': (val_left < val_right),
			'<=': (val_left <= val_right),
			'>': (val_left > val_right),
			'>=': (val_left >= val_right),
			'==': (val_left == val_right),
			'!=': (val_left != val_right),
			}
			return switch.get(argument,"False")
		casestore = switchcase(self.op)
		
		if (casestore == "False"):
			print ('Unknown operator:', self.op)
			sys.exit()
			
		return casestore

class bool_and(bool_class):
	def __init__(self, left, right):
		self.left = left
		self.right = right

	def loop_act(self):
		return (self.left, self.right)

	def process(self, val):
		val_left = self.left.process(val)
		val_right = self.right.process(val)
		return (val_left and val_right)

class bool_or(bool_class):
	def __init__(self, left, right):
		self.left = left
		self.right = right

	def loop_act(self):
		return (self.left, self.right)

	def process(self, val):
		val_left = self.left.process(val)
		val_right = self.right.process(val)
		return (val_left or val_right)

class bool_not(bool_class):
	def __init__(self, state):
		self.state = state

	def loop_act(self):
		return self.state

	def process(self, val):
		value = not self.state.process(val)
		return value

class parse_class:
	def __add__(self, other):
		return mult_token(self, other)
	
	def __mul__(self, other):
		return parse_multihelper(self, other)

	def __or__(self, other):
		return parse2_token(self, other)

	def __xor__(self, func):
		return proc(self, func)

class tag_gen(parse_class):
	def __init__(self, tag):
		self.tag = tag

	def __call__(self, tokens, pos):
		if pos < len(tokens) and tokens[pos][1] is self.tag:
			return output(tokens[pos][0], pos + 1)
		else:
			return None

class op_parse(parse_class):
	def __init__(self, value, tag):
		self.value = value
		self.tag = tag

	def __call__(self, tokens, pos):
		if pos < len(tokens) and tokens[pos][0] == self.value and tokens[pos][1] is self.tag:
			return output(tokens[pos][0], pos + 1)
		else:
			return None

class mult_token(parse_class): 
	def __init__(self, tok1, tok2):
		self.tok1 = tok1
		self.tok2 = tok2

	def __call__(self, tokens, pos):
		left_result = self.tok1(tokens, pos)
		if left_result:
			right_result = self.tok2(tokens, left_result.pos)
			if right_result:
				comb_result = (left_result.value, right_result.value)
				return output(comb_result, right_result.pos)
		return None

class parse2_token(parse_class):
	def __init__(self, left, right):
		self.left = left
		self.right = right

	def __call__(self, tokens, pos):
		left_result = self.left(tokens, pos)
		if left_result:
			return left_result
		else:
			right_result = self.right(tokens, pos)
			return right_result
		
class parse_multihelper(parse_class):
	def __init__(self, par, seperate):
		self.par = par
		self.seperate = seperate

	def __call__(self, tokens, pos):
		res = self.par(tokens, pos)

		def process_next(parsed):
			(seprate, right) = parsed
			return seprate(res.value, right)
		nextPar = self.seperate + self.par^process_next

		nextRes = res
		while nextRes:
			nextRes = nextPar(tokens, res.pos)
			if nextRes:
				res = nextRes
		return res            

class if_parse(parse_class): #Used to parse if statements
	def __init__(self, par):
		self.par = par

	def __call__(self, tokens, pos):
		res = self.par(tokens, pos)
		if res:
			return res
		else:
			return output(None, pos)

class proc(parse_class): #Used to parse tokens and create AST nodes from mult_token
	def __init__(self, par, func):
		self.par = par
		self.func = func

	def __call__(self, tokens, pos):
		res = self.par(tokens, pos)
		if res:
			res.value = self.func(res.value)
			return res

class token_parstail(parse_class):
	def __init__(self, function):
		self.par = None
		self.function = function

	def __call__(self, tokens, pos):
		if not self.par:
			self.par = self.function()
		return self.par(tokens, pos)

#unused tokens
class token_filter(parse_class):
	def __init__(self, par):
		self.par = par

	def __call__(self, tokens, pos):
		res = self.par(tokens, pos)
		if res and res.pos == len(tokens):
			return res
		else:
			return None
		
class output:
	def __init__(self, value, pos):
		self.value = value
		self.pos = pos

	def rep(self):
		return (self.value, self.pos)
		
def keyword_gen(key):
	return op_parse(key, OPR)

num = tag_gen(INT)^(lambda i: int(i))
string_act = tag_gen(STR)^(lambda s: string_class(s))
idt = tag_gen(IDT)

arProg = [['*', '/'],['+', '-']] #Arithemetic operators

boolProg = [['and'],['or']] #Boolean operators

def parse_tokens(tokens):
	asynt = parse_start()(tokens, 0)
	return asynt

def parse_start():
	return token_filter(state_parse())

def state_parse():
	sep = keyword_gen(';')^(lambda x: lambda l, r: mult_declare(l, r))
	return parse_multihelper(parse_constructassign(), sep)

def parse_constructassign():
	return parse_stateassign()|\
		   state_if()|state_while()|state_for()

def parse_stateassign(): #Assignment
	def process(parse_exp):
		((name, _), exp) = parse_exp
		return var_assign(name, exp)
	return idt + keyword_gen(':=') + parse_arith()^process

def state_if():
	def process(parse_exp):
		(((((_, condition), _), true), falseR), _) = parse_exp
		if falseR:
			(_, false) = falseR
		else:
			false = None
		return if_actual(condition, true, false)
	return keyword_gen('if') + parse_bool() + keyword_gen('then') + token_parstail(state_parse) + if_parse(keyword_gen('else') + token_parstail(state_parse)) + keyword_gen('end')^process

def state_for():
	def process(parse_exp):
		((((((_, condition), _), body), _), sequence), _) = parse_exp
		return for_actual(condition, body, sequence)
	return keyword_gen('for') + idt + keyword_gen (':=') + parse_arith() + keyword_gen('to') + parse_arith() + keyword_gen('do') + token_parstail(state_parse) + keyword_gen('end')^process


def state_while():
	def process(parse_exp):
		((((_, condition), _), body), _) = parse_exp
		return while_actual(condition, body)
	return keyword_gen('while') + parse_bool() + keyword_gen('do') + token_parstail(state_parse) + keyword_gen('end')^process

def parse_bool():
	return comp_before(parse_boolsec(), boolProg, abool_actual)

def parse_boolsec():
	return parse_not()|parse_comparison()|parse_structb()

def parse_not():
	return keyword_gen('not') + token_parstail(parse_boolsec)^(lambda parse_exp: NotBexp(parse_exp[1]))

def parse_comparison():
	list = ['>', '<', '>=', '<=', '!=', '=']
	return parse_arith() + op_list(list) + parse_arith()^parse_arithmult

def parse_structb():
	return keyword_gen('(') + token_parstail(parse_bool) + keyword_gen(')')^ar_struct

def parse_arith():
	return comp_before(parse_arithsel(),arProg,parse_arith2)

def parse_arithsel():
	return parse_arithval() | parse_arithstruct()

def parse_arithstruct():
	return keyword_gen('(') + token_parstail(parse_arith) + keyword_gen(')')^ar_struct

def parse_arithval():
	return (num^(lambda i: integer_class(i)))|(string_act^(lambda s: string_class(s))|(idt^(lambda v: var_class(v))))

def comp_before(valPars, priorLevel, combine):
	def opPars(beforev):
		return op_list(beforev)^combine
	parse_start = valPars * opPars(priorLevel[0])
	for beforev in priorLevel[1:]:
		parse_start = parse_start * opPars(beforev)
	return parse_start

def parse_arith2(op_parse):
	return lambda l, r: arith_actual(op_parse, l, r)

def parse_arithmult(parse_exp):
	((left, op), right) = parse_exp
	return bool_comparison(op, left, right)

def abool_actual(op_parse):
	if op_parse == 'and':
		return lambda l, r: bool_and(l, r)
	elif op_parse == 'or':
		return lambda l, r: bool_or(l, r)
	else:
		print("Unknown operator: ", op_parse)
		sys.exit()

def ar_struct(parse_exp):
	((_, str), _) = parse_exp
	return str

def op_list(ops):
	opl = [keyword_gen(op) for op in ops]
	parse_start = reduce(lambda space, reduced: space | reduced, opl)
	return parse_start
