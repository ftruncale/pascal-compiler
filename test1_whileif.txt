a := 7;
b := 1;
c := "initial string";
while (a > 0) do
  b := b * a;
  a := a - 2;
if (a < 0) then
  c := "all done"
else
  c := "something went wrong"
end
end