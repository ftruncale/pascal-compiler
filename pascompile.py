'''
Fran Truncale
Further documentation available in the README
'''

from core import *

def main():
	if len(sys.argv) < 2:
		print("Missing input argument.")
		sys.exit()

	filename = sys.argv[1]
	with open(filename,"r") as file_handler:
		code=file_handler.read()
		file_handler.close()

	output = parse_tokens(token_gen(code))
	if not output:
		print("[!] Parsing error. [!]")
		sys.exit()

	asynt = output.value
	value = {}
	asynt.process(value)
	for var in value:
		print ("VAR:",var,value[var])


main()
